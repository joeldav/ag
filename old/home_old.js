angular.module('app.controllers').controller('HomeCtrl', function($scope, $state, $injector, $document, $http, $location, $translate, sharedProperties) {

    sharedProperties.setVideo(document.getElementById("the-video"));

    var video = sharedProperties.getVideo();


    $scope.changeLanguage = function (key,consentAge) {

        $translate.use(key);
        $scope.translationData = {
            consent: consentAge
        };

    };

    if(sharedProperties.getShown()==true) {

        showFinish(0);
        $scope.$apply(function() {
            $scope.showScroll = true;
        });
    }

    if(BrowserDetect.browser == 'Explorer') {
        jQuery('.finish').fadeIn(100);
    }

    function showFinish(time) {

        jQuery('.finish').fadeIn(time);
        sharedProperties.setShown(true);
        jQuery('.scroll-down-container').fadeIn(100);
        jQuery('.scroll-down-container').removeClass('ng-hide');
        $scope.showScroll = true;
    }

    function showFragrance() {
        var element = document.getElementById('fragrance');
        jQuery('#fragrance').show();
        jQuery('#fragrance,#fragrance .textSection').css('height',jQuery(window).height());
        $document.duScrollTo(element,null,800);
        //$scope.$apply(function() {
         //   $scope.showScroll = false;
        //});
    }

    // remove padding
    jQuery('.ui-view-container').removeClass('ui-padded');

    // competition form - make a copy so we can set the form back to pristine later

    $scope.formData = {};
    $scope.master= {};

    $scope.update = function(formData) {
        $scope.master= angular.copy(formData);
        if ($scope.form) $scope.form.$setPristine();
    };

    $scope.reset = function() {
        jQuery('.form-part-1').show(0);
        jQuery('.form-part-2').hide();
        $scope.formData = angular.copy($scope.master);
        if ($scope.form) $scope.form.$setPristine();
    };

    // process the form
    $scope.processForm = function() {
        jQuery('#signupLoader').show();
        //
        $http.post('/api/process.php', $scope.formData) .success(function(data) {
            if (!data.success) {
                jQuery('#signupLoader').hide();
                // if not successful, bind errors to error variables
                $scope.errorName = data.errors.name;
                $scope.errorAge = data.errors.age;
                $scope.errorCountry = data.errors.country;
                $scope.errorEmail = data.errors.email;
                $scope.errorOver18 = data.errors.over18;
                $scope.errorTerms1 = data.errors.terms1;
                $scope.errorSingle = data.errors.single;

            } else {
                jQuery('#signupLoader').hide();
                jQuery('#ticket').hide();
                // if successful, bind success message to message
                $scope.message = data.message;
            }
        });
    };

    $scope.consentChange = function(event) {
        $event.currentTarget

    }

    $scope.scrollDown = function() {
        showFragrance();
       // $scope.showScroll=false;

    }

    // signup scroll and fill
    $scope.signup = function() {
        $scope.reset();

        $scope.formData.age = 18;
        jQuery('#signup').show();
        jQuery('.signup-overlay').show();
        jQuery('#signup').addClass('signup-active');

    };

    // close button
    $scope.close = function(element) {
        var element = document.getElementById('signup');
        jQuery('#signup').hide();
        jQuery('.signup-overlay').hide();
        jQuery('#signup').removeClass('signup-active');

    };

    // mobile play button
    $scope.playMobile = function(element) {

        video.webkitEnterFullScreen();
        video.play();
        showFinish(500);

    };




    // country selection
    $scope.country = function($event) {

        jQuery('.the-terms').attr('ng-href','/#terms-row');

        var countryVal = jQuery($event.currentTarget).data('val');

        if(countryVal=="Other") {
            jQuery('.win').empty();
            jQuery('.country-flags').empty();
            jQuery('.win').append('<p>This competition is not open to other countries, thank you for your interest</p>');
            return false;
        }

        if(countryVal=="US") {
            jQuery('.the-terms').attr('ng-href','/#terms-us');
        }
        if(countryVal=="FR") {
            jQuery('.the-terms').attr('ng-href','/#terms-fr');
        }


        jQuery('.country-flags li').removeClass('active');
        $scope.formData.country = countryVal;
        jQuery($event.currentTarget).addClass('active');

        jQuery('.form-part-1').hide(0);
        jQuery('.form-part-2').show();

        $scope.changeLanguage(jQuery($event.currentTarget).data('lang'),jQuery($event.currentTarget).data('consent'));
    };


    // initialize video
    $scope.$on('$viewContentLoaded', function(){


        Modernizr.addTest('inpagevideo', function ()
        {
            var inpagevideo = navigator.userAgent.match(/(iPhone|iPod)/g) ? false : true;

            if(inpagevideo==false) {
                showFinish(500);
            }

            return inpagevideo;
        });

        Modernizr.addTest('apple', function ()
        {
            var apple = navigator.userAgent.match(/(iPhone|iPod|iPad)/g) ? false : true;

            if(apple==false) {
                jQuery('#the-video').attr('loop', false);
            }
        });


        if (Modernizr.video) {


            var videoPlayed = 0;
            jQuery('#the-video').attr('controls', false);
            jQuery('#the_video').css('width','100%');
            jQuery('#the_video').css('height','100%');


            video.addEventListener( "canplay", function() {
                video.play();
                video.volume = sharedProperties.getVolume();
            });

            //  Capture time changes and display current position
            video.addEventListener("timeupdate", function () {
                var time = video.currentTime.toFixed(2) ;

                if(time > 10 && videoPlayed != 1) {
                    showFinish(1000);
                    videoPlayed=1;

                }
            }, false);


        } else {

            showFinish(0);
        }


    });

});