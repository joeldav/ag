# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Joel Davey -- Developer -- joel.davey

# TECHNOLOGY COLOPHON

    HTML5, CSS3
    jQuery, Modernizr, Angular, Foundation 5
