<?php
// process.php

include('mailchimp.php');


function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

$errors = array();  // array to hold validation errors
$data = array();        // array to pass back data

// validate the variables ========
if (empty($_POST['firstname']))
    $errors['firstname'] = 'First name is required.';

if (empty($_POST['lastname']))
    $errors['lastname'] = 'Last name is required.';

if (empty($_POST['over13']) || $_POST['over13']=="false")
    $errors['over13'] = 'You must be over 13';

if (empty($_POST['terms']) || $_POST['terms']=="false")
    $errors['terms'] = 'Please accept the terms and conditions.';

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) || empty($_POST['email'])) {
    $errors['email'] = "Invalid email address";
}
// return a response ==============

// response if there are errors
if ( ! empty($errors)) {

    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

    // if there are no errors, return a message

    $MailChimp = new Drewm\MailChimp('81ec982a0b3ea22a948dd100ab146eac-us11');
    $result = $MailChimp->call('lists/subscribe', array(
        'id'                => 'ff241d5da5',
        'email'             => array('email'=>$_POST['email']),
        'merge_vars'        => array(

            'FIRSTNAME'=>$_POST['firstname'],
            'LASTNAME'=>$_POST['lastname'],
            'OVER13'=>$_POST['over13'],
            'TERMS'=>$_POST['terms'],
            'BROWSER'=>$_SERVER['HTTP_USER_AGENT'],
            'IP'=>$_SERVER['HTTP_X_FORWARDED_FOR'],
        ),
        'double_optin'      => false,
        'update_existing'   => true,
        'replace_interests' => false,
        'send_welcome'      => false,
    ));



    $data['success'] = true;
    $data['message'] = 'Thank you for signing up, lookout for my email in your inbox';
}

// return all our data to an AJAX call
echo json_encode($data);