angular.module('app.services', [])
    .service('sharedProperties', function () {

        var video,shown;
        var volume = 1;

        return {
            getVideo: function () {
                return video;
            },
            setVideo: function(value) {
                video = value;
            },
            getVolume: function () {
                return volume;
            },
            setVolume: function(value) {
                volume = value;
            },
            getShown: function () {
                return shown;
            },
            setShown: function(value) {
                shown = value;
            }
        };

    });