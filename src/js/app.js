(function () {
    'use strict';

    // jQuery
    jQuery.noConflict();

    angular.module('app', ['ui.router', 'app.controllers', 'app.services', 'duScroll', 'oc.lazyLoad'])

        .run(function ($rootScope) {




            $rootScope.$on('$stateChangeStart', function (event, next) {


            });


        }).run(function ($rootScope) {
            $rootScope.$on('$viewContentLoaded', function (event, next) {



                // run foundation
                jQuery(document).foundation();

            });
            $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
                $rootScope.containerClass = toState.containerClass;
            });

        })

        .config(['$httpProvider', function ($httpProvider) {
            // Intercept POST requests, convert to standard form encoding
            $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $httpProvider.defaults.transformRequest.unshift(function (data, headersGetter) {
                var key, result = [];
                for (key in data) {
                    if (data.hasOwnProperty(key)) {
                        result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
                    }
                }
                return result.join("&");
            });
        }])
        .config(function ($locationProvider, $stateProvider, $urlRouterProvider) {

            $stateProvider

                .state('home', {
                    url: '/',
                    templateUrl: 'views/home.html',
                    controller: 'HomeCtrl'

                })
                .state('home2', {
                    url: '',
                    templateUrl: 'views/home.html',
                    controller: 'HomeCtrl'

                })
                .state('product', {
                    url: '/fragrance',
                    templateUrl: 'views/product.html',
                    controller: 'ProductCtrl',
                    containerClass: 'product'
                })
                .state('404', {
                    url: '/404',
                    templateUrl: 'views/404.html',
                    controller: 'RootCtrl'
                })
                .state('terms', {
                    url: '/terms',
                    templateUrl: 'views/terms.html',
                    controller: 'RootCtrl'
                })
                .state('privacy', {
                    url: '/privacy',
                    templateUrl: 'views/privacy.html',
                    controller: 'RootCtrl'
                });
            // use the HTML5 History API
            //$locationProvider.html5Mode(true);
            //$locationProvider.hashPrefix('!');
            $urlRouterProvider.otherwise('404');


        });

}());


