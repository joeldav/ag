angular.module('app.controllers').controller('HomeCtrl', function($scope, $state, $injector, $document, $http, $location, sharedProperties) {


    // remove padding
    //jQuery('.ui-view-container').removeClass('ui-padded');
    // competition form - make a copy so we can set the form back to pristine later



    sharedProperties.setVideo(document.getElementById("the-video"));
    var video = sharedProperties.getVideo();

    $scope.formData = {};
    $scope.master= {};

    $scope.update = function(formData) {
        $scope.master= angular.copy(formData);
        if ($scope.form) $scope.form.$setPristine();
    };

    $scope.reset = function() {
        jQuery('#ticket').show();
        $scope.formData = angular.copy($scope.master);
        if ($scope.form) $scope.form.$setPristine();
    };

    // process the form
    $scope.processForm = function() {
        jQuery('.loaderContainer').show();
        $http.post('/api/process.php', $scope.formData) .success(function(data) {
            if (!data.success) {
                jQuery('.loaderContainer').hide();
                // if not successful, bind errors to error variables
                $scope.errorFirstName = data.errors.firstname;
                $scope.errorLastName = data.errors.lastname;
                $scope.errorEmail = data.errors.email;
                $scope.errorOver13 = data.errors.over13;
                $scope.errorTerms = data.errors.terms;


            } else {
                jQuery('.loaderContainer').hide();
                jQuery('#ticket').hide();
                // if successful, bind success message to message
                $scope.message = data.message;
            }
        });
    };





    // play button
    $scope.play = function(event) {

        event.preventDefault();
        var element = jQuery('.wistia-section').html();

        wistiaEmbed = Wistia.embed("9xxq4i5nui", {
            playerColor: "aa87b1",
            fullscreenButton: true,
            videoHeight:500,
            container: "wistia1",
            controlsVisibleOnLoad: false,
            chromeless: true,
            videoFoam: true
        });

        wistiaEmbed.play();

        wistiaEmbed.bind('end', function() {
            // use the .time() method to jump ahead 10 seconds
           // jQuery('.wistia-section').html(element);
            //jQuery('.wistia-section').html(element);
            wistiaEmbed.remove();
            jQuery('#wistia1').css('height','0px');

            return this.unbind;
        });

    };


});