(function () {
    'use strict';
    // this function is strict...
}());
angular.module('app.controllers').controller('ProductCtrl', function($scope, $state, sharedProperties) {

    //sharedProperties.setVideo(document.getElementById("the-video"));

    var dataNodes = {

        0: {
            img: "/assets/img/bottle-cutout.png",
            text: "The luscious new fragrance opens with sparkling fruits and an ultra-feminine floralcy, passionately spun with musks, woods, and an addictive hint of marshmallow.",
            title: "EDP"
        },
        1: {
            img: "/assets/img/hair-mist.png",
            text: "Up, down and all around, the ARI by ARIANA GRANDE scented Hair Mist is the perfect remedy for feel-good, smell-good, and look-good hair!",
            title: "Hair Mist"
        },
        2: {
            img: "/assets/img/body-lotion.png",
            text: "Apply your body lotion and you’ll be good to go, this ARI by ARIANA GRANDE Body Lotion is the perfect essential to add to your bag.",
            title: "Body Lotion"
        },
        3: {
            img: "/assets/img/giftset.png",
            text: "Ultra-feminine and flirty, this ARI by ARIANA GRANDE gift set features a 30ml Eau de Parfum Spray and 100ml Body Lotion in a quilted gift box.",
            title: "Gift Set"
        },

    };


    $scope.loadProduct = function($event) {

        var element = $event.currentTarget;
        var node = jQuery(element).data('node');

        jQuery('.product-nav').removeClass('active');
        jQuery('.product-title').text(dataNodes[node].title);
        jQuery('.product-description').text(dataNodes[node].text);
        jQuery('.product-image-img').attr('src',(dataNodes[node].img));
        jQuery(element).addClass('active');


    }


    jQuery('.ui-view-container').addClass('ui-padded');
   // jQuery('body').data('role',''+$state.current.name+'');



        jQuery('.ui-view-container').removeClass('ui-padded');
        jQuery( window ).resize(function() {

            var body = jQuery('body').height();
            var newHeight = body-140;
            if(newHeight > 780) {
                jQuery('.panel').css('height', newHeight + "px");
            }


        });

        window.dispatchEvent(new Event('resize'));



    //back to top
    jQuery(document).ready(function($){



            addthis.init();
            addthis.toolbox('.addthis_sharing_toolbox');

        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
            scroll_top_duration = 700,
        //grab the "back to top" link
            $back_to_top = $('.cd-top');

        //hide or show the "back to top" link
        $(window).scroll(function(){
            ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if( $(this).scrollTop() > offset_opacity ) {
                $back_to_top.addClass('cd-fade-out');
            }
        });

        //smooth scroll to top
        $back_to_top.on('click', function(event){
            event.preventDefault();
            $('body,html').animate({
                    scrollTop: 0 ,
                }, scroll_top_duration
            );
        });

    });




});