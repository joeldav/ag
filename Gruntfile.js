(function () {
    'use strict';
    // this function is strict...
}());

module.exports = function (grunt) {

    var LOCAL_PATH = 'dev';
    var RELEASE_PATH = 'live';

    global.outputPath = "live";

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['<%= grunt.option(\"target\") %>/app/','.sass-cache'],
        concat: {
            // the files to concatenate
            app: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n',
                    stripBanners: true
                },
                src: [
                    //'src/bower_components/es5-shim/es5-shim.min.js',
                    'src/bower_components/angular/angular.min.js',
                    'src/bower_components/angular-translate/angular-translate.min.js',
                    'src/bower_components/json3/lib/json3.min.js',
                    'src/bower_components/angular-touch/angular-touch.min.js',
                    'src/bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'src/bower_components/oclazyload/dist/ocLazyLoad.min.js'
                    //'src/bower_components/angular-addthis/dist/angular-addthis.min.js'
                    // angular material
                    //'src/bower_components/angular-aria/angular-aria.min.js',
                    //'src/bower_components/angular-animate/angular-animate.min.js',
                    //'src/bower_components/angular-material/angular-material.js',
                    // app
                    //'src/bower_components/ngSmoothScroll/angular-smooth-scroll.js',
                ],
                // the location of the resulting JS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/js/app.js',
                nonull: true
            },
            index: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n',
                    stripBanners: true
                },
                src: [
                    'src/bower_components/angular-scroll/angular-scroll.min.js',
                    'src/bower_components/vminpoly/tokenizer.js',
                    'src/bower_components/vminpoly/parser.js',
                    'src/bower_components/vminpoly/vminpoly.js',
                    'src/bower_components/jquery/dist/jquery.min.js',
                    'src/bower_components/foundation/js/foundation.min.js',
                    'src/js/modules/*.js',
                    'src/js/plugins/*.js',
                    'src/js/*.js',
                    'src/js/controllers.js',
                    'src/js/controllers/*.js',
                    'src/js/services.js',
                    'src/js/services/*.js'
                ],
                // the location of the resulting JS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/js/index.js',
                nonull: true
            },
            css: {

                // the files to concatenate
                src: [
                    //'src/bower_components/foundation/css/foundation.css',
                    //'src/bower_components/angular-material/angular-material.min.css',
                    'src/css/foundation.css',
                    'src/bower_components/loaders.css/loaders.css',
                    'src/bower_components/animate.css/animate.min.css',
                ],
                // the location of the resulting CSS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/css/main.css',
                nonull: true

            },
            cssapp: {

                // the files to concatenate
                src: [
                    'src/css/app.css',
                ],
                // the location of the resulting CSS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/css/app.css',
                nonull: true

            },
            modernizr: {
                // the files to concatenate
                src: [
                    'src/bower_components/modernizr/modernizr.js'
                ],
                // the location of the resulting JS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/js/modernizr.js',
                nonull: true
            }


        },
        sass: {
            dist: {
                options: {                       // Target options
                    style: 'expanded',
                    loadPath: ['src/bower_components/foundation/scss']
                },
                files: {
                    'src/css/foundation.css': 'src/css/foundation/settings.scss'
                }
            },
            main: {
                files: {
                    'src/css/app.css': 'src/css/app.scss'
                }
            }
        },

        cssmin: {
            css: {
                src: '<%= grunt.option(\"target\") %>/app/assets/css/main.css',
                dest: '<%= grunt.option(\"target\") %>/app/assets/css/main.css'
            },
            app: {
                src: '<%= grunt.option(\"target\") %>/app/assets/css/app.css',
                dest: '<%= grunt.option(\"target\") %>/app/assets/css/app.css'
            }
        },

        jshint: {
            files: ['Gruntfile.js', 'src/js/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },
        copy: {
            main: {
                files: [{
                    nonull: true,
                    cwd: 'src/',
                    expand: true,
                    src: [
                        "*.png",
                        "*.xml",
                        "*.txt",
                        "*.ico",
                        ".htaccess",
                        "index.html",
                        "views/**",
                        "errors/**"],
                    dest: "<%= grunt.option(\"target\") %>/app/"
                },
                    {
                        cwd: 'src/',
                        nonull: true,
                        expand: true,
                        src: [
                            "img/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    },
                    {
                        cwd: 'src/',
                        nonull: true,
                        expand: true,
                        src: [
                            "video/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    },
                    {
                        cwd: 'src/',
                        nonull: true,
                        expand: true,
                        src: [
                            "fonts/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    },
                    {
                        cwd: 'src/',
                        nonull: true,
                        expand: true,
                        src: [
                            "api/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/"
                    }],
            },
        },
        watch: {
            options: {
                livereload: true,
            },
            jshint: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint']
            },
            html: {
                files: ['src/**'],
                tasks: ['sass','concat', 'cssmin', 'copy']
            }

        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    hostname: 'localhost',
                    base: '<%= grunt.option(\"target\") %>/app',
                    keepalive: true
                }
            }
        },
        gitaddd: {
            your_target: {
                options: {
                    cwd: "/src"
                },
                files: [
                    {
                        src: ['src/**'],
                        expand: true,
                        cwd: "/src"
                    }
                ]
            }
        },
        gitcommit: {
            your_target: {
                options: {
                    cwd: "/src"
                },
                files: [
                    {
                        src: ['src/**'],
                        expand: true,
                        cwd: "/src"
                    }
                ]
            }
        },
        cdnify: {
            someTarget: {
                options: {
                    base: '//cdn.arianagrandefragrances.com'
                },
                files: [{
                    expand: true,
                    cwd: '<%= grunt.option(\"target\") %>/app',
                    src: '**/*.{css,html}',
                    dest: '<%= grunt.option(\"target\") %>/app'
                }]
            }
        },
        htmlmin: {                                     // Task
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                expand: true,
                cwd: '<%= grunt.option(\"target\") %>/app',
                src: ['**/*.html'],
                dest: '<%= grunt.option(\"target\") %>/app/'
            }
        },
        uglify: {

            options: {
                mangle: false
            },
            my_target: {
                files: {
                    '<%= grunt.option(\"target\") %>/app/assets/js/index.js': ['<%= grunt.option(\"target\") %>/app/assets/js/index.js'],
                    '<%= grunt.option(\"target\") %>/app/assets/js/app.js': ['<%= grunt.option(\"target\") %>/app/assets/js/app.js']
                }
            }
        },
        svgmin: {
            options: {
                plugins: [
                    {
                        removeViewBox: false
                    }, {
                        removeUselessStrokeAndFill: false
                    }
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= grunt.option(\"target\") %>/app/assets/img',
                    src: '*.svg',
                    dest: '<%= grunt.option(\"target\") %>/app/assets/img'
                }]
            }
        }

    });

    //grunt tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-badass');
    grunt.loadNpmTasks('grunt-angular-gettext');
    grunt.loadNpmTasks('grunt-cdnify');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-svgmin');

    grunt.registerTask("setOutput", "Set the output folder for the build.", function () {
        if (global.buildType === "local") {
            global.outputPath = LOCAL_PATH;
        }
        if (global.buildType === "release") {
            global.outputPath = RELEASE_PATH;
        }
        if (grunt.option("target")) {
            global.outputPath = grunt.option("target");
        }

        grunt.option("target", global.outputPath);
        grunt.log.writeln("Output path: " + grunt.option("target"));
    });

    grunt.registerTask('dev', function() {
        global.outputPath = "dev";
        grunt.task.run(['setOutput', 'clean', 'jshint', 'sass', 'concat', 'copy']);
    });

    grunt.registerTask('preview', function() {
        global.outputPath = "dev";
        grunt.task.run(['setOutput', 'clean', 'jshint', 'sass', 'concat', 'cssmin', 'copy','uglify','htmlmin','svgmin']);
    });

    grunt.registerTask('release', function() {
        global.outputPath = "live";
        grunt.task.run(['setOutput', 'clean', 'jshint', 'sass', 'concat', 'cssmin', 'copy','uglify','htmlmin', 'cdnify', 'svgmin']);
    });

};
